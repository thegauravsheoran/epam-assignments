//Task 1
function reverseNumber(num) {
    let reversed = 0;
    let isNegative=0;
  
    if (num < 0) {
      isNegative = true;
      num = -num;
    } else {
      isNegative = false;
    }
  
    while (num > 0) {
      let remainder = num % 10;
      reversed = reversed * 10 + remainder;
      num = (num - remainder) / 10;
    }
  
    if (isNegative) {
      return -reversed;
    } else {
      return reversed;
    }
  }
  
  console.log(reverseNumber(12345)); // returns 54321
  console.log(reverseNumber(-56789)); // returns -98765


  //Task 2
  function forEach(arr, func) {
    for (let i = 0; i < arr.length; i++) {
      func(arr[i]);
    }
  }

  
  forEach([2, 5, 8], function(el) {
    console.log(el);
  });
  // logs to console: 2 5 8

  
  //Task 3
  function map(arr, func) {
    const result = [];
    forEach(arr, function(el) {
      result.push(func(el));
    });
    return result;
  }

  
  console.log(map([2, 5, 8], function(el){
     return el + 3; 
  }));
  // returns [5, 8, 11]

  console.log(map([1, 2, 3, 4, 5], function(el) { 
    return el * 2; 
  }));
  // returns [2, 4, 6, 8, 10]

//Task 4
function filter(arr, func) {
    const result = [];
    forEach(arr, function(el) {
      if (func(el)) {
        result.push(el);
      }
    });
    return result;
  }

  console.log(filter([2, 5, 1, 3, 8, 6], function(el){ 
    return el > 3 
  }));
  // returns [5, 8, 6]

  console.log(filter([1, 4, 6, 7, 8, 10], function(el){ 
    return el % 2 === 0 
   }
  ));
  // returns [4, 6, 8, 10]


//Task 5
function getAdultAppleLovers(data) {
    const filteredData = filter(data, function(person) {
      return person.age > 18 && person.favoriteFruit === 'apple';
    });
    return map(filteredData, function(person) {
      return person.name;
    });
  }

  const data = [
    {
    '_id': '5b5e3168c6bf40f2c1235cd6',
    'index': 0,
    'age': 39,
    'eyeColor': 'green',
    'name': 'Stein',
    'favoriteFruit': 'apple'
    },
    {
    '_id': '5b5e3168e328c0d72e4f27d8',
    'index': 1,
    'age': 38,
    'eyeColor': 'blue',
    'name': 'Cortez',
    'favoriteFruit': 'strawberry'
    },
    {
    '_id': '5b5e3168cc79132b631c666a',
    'index': 2,
    'age': 2,
    'eyeColor': 'blue',
    'name': 'Suzette',
    'favoriteFruit': 'apple'
    },
    {
    '_id': '5b5e31682093adcc6cd0dde5',
    'index': 3,
    'age': 17,
    'eyeColor': 'green',
    'name': 'Weiss',
    'favoriteFruit': 'banana'
    }
   ];
  
  console.log(getAdultAppleLovers(data)); // returns ['Susan', 'Stein']

  //Task 6
  function getKeys(obj) {
    const keys = [];
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
      keys.push(key);
    }
  }
    return keys;
  }

  const obj = { keyOne: 1, keyTwo: 2, keyThree: 3 };
  console.log(getKeys(obj)); // returns ['keyOne', 'keyTwo', 'keyThree']


//Task 7
function getValues(obj) {
    const values = [];
    
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        values.push(obj[key]);
        }
    }
    return values;
  }

  const obj1 = { keyOne: 1, keyTwo: 2, keyThree: 3 };
  console.log(getValues(obj1)); // returns [1, 2, 3]
