//task 1
function isEquals(a, b) {
    return a === b;
  }

console.log(isEquals(3, 3)); // true
console.log(isEquals(3, "3")); // false  
  
//Task 2
function isBigger(a, b) {
    return a > b;
  }
  console.log(isBigger(5, -1)); // true
  console.log(isBigger(2, 2)); // false

//Task 3
function storeNames(...names) {
    return names;
  }

  console.log(storeNames('Tommy Shelby', 'Ragnar Lodbrok', 'Tom Hardy'));
// ['Tommy Shelby', 'Ragnar Lodbrok', 'Tom Hardy']


//Task 4
function getDifference(a, b) {
    if (a < b) {
      [a, b] = [b, a]; // Swap the values of a and b
    }
    return a - b;
  }
console.log(getDifference(5, 3)); // 2
console.log(getDifference(5, 8)); // 3

//Task 5
function negativeCount(arr) {
    let count = 0;
    for (let i = 0; i < arr.length; i++) {
      if (arr[i] < 0) {
        count++;
      }
    }
    return count;
  }
  console.log(negativeCount([4, 3, 2, 9])); // 0
console.log(negativeCount([0, -3, 5, 7])); // 1

  
//Task 6
function letterCount(str, letter) {
    let count = 0;
    for (let i = 0; i < str.length; i++) {
      if (str[i] === letter) {
        count++;
      }
    }
    return count;
  }
  console.log(letterCount("Marry", "r")); // 2

  
//Task 7
function countPoints(games) {
    let points = 0;
    for (let i = 0; i < games.length; i++) {
      let x = 0;
      let y = 0;
      let score = '';
      for (let j = 0; j < games[i].length; j++) {
        if (games[i][j] === ':') {
          x = parseInt(score);
          score = '';
        } else {
          score += games[i][j];
        }
      }
      y = parseInt(score);
      if (x > y) {
        let num=3;
        points += num;
      } else if (x === y) {
        points += 1;
      }
    }
    return points;
  }
  

console.log(countPoints(['100:90', '110:98', '100:100', '95:46', '54:90', '99:44', '90:90', '111:100'])); // 17
