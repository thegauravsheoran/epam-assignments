/* START TASK 1: Your code goes here */

const table = document.querySelector('table');
const cells = document.querySelectorAll('td');

let selectedCells = [];
let selectedRows = [];

function toggleSelectedCell(cell) {
  if (cell.classList.contains('selected')) {
    cell.classList.remove('selected');
    cell.style.backgroundColor = '';
    selectedCells.splice(selectedCells.indexOf(cell), 1);
  } else {
    cell.classList.add('selected');
    cell.style.backgroundColor = 'yellow'; 
    cell.style.backgroundColor = 'blue';  
    selectedCells.push(cell);
  }
}

function toggleSelectedRow(row) {
  if (row.classList.contains('selected-row')) {
    row.classList.remove('selected-row');
    row.style.backgroundColor = '';
    selectedRows.splice(selectedRows.indexOf(row), 1);
  } else {
    row.classList.add('selected-row');
    row.style.backgroundColor = 'blue';
    selectedRows.push(row);
  }
}

function toggleTableColor() {
  if (table.classList.contains('selected')) {
    table.classList.remove('selected');
    cells.forEach(cell => {
      if (!cell.classList.contains('selected')) {
        cell.style.backgroundColor = '';
      }
    });
    selectedCells = [];
    selectedRows.forEach(row => {
      if (!row.classList.contains('selected-row')) {
        row.style.backgroundColor = '';
      }
    });
    selectedRows = [];
  } else {
    table.classList.add('selected');
    cells.forEach(cell => {
      if (!cell.classList.contains('selected')) {
        cell.classList.add('selected');
        cell.style.backgroundColor = 'yellow';
        selectedCells.push(cell);
      }
    });
  }
}

cells.forEach(cell => {
  cell.addEventListener('click', () => {
    if (cell.classList.contains('special')) {
      toggleTableColor();
    } else {
      toggleSelectedCell(cell);
      if (cell.classList.contains('col-1')) {
        const row = cell.parentNode;
        toggleSelectedRow(row);
      }
    }
  });
});

table.addEventListener('click', function(event) {
  const clickedCell = event.target;
  if (clickedCell.cellIndex === 0) {
    const row = clickedCell.parentNode;
    if (!row.classList.contains('selected-row')) {
      toggleSelectedRow(row);
    }
  }
});


/* END TASK 1 */

/* START TASK 2: Your code goes here */
const input = document.querySelector('#phone-number');
const button = document.querySelector('#send-button');
const notification = document.querySelector('#notification');

// Function to validate the phone number
function validatePhoneNumber(phoneNumber) {
  const regex = /^\+380\d{9}$/;
  return regex.test(phoneNumber);
}

// Function to show the invalid data notification
function showInvalidDataNotification() {
  notification.textContent = 'Invalid data. Please enter a valid phone number in the format +380*********';
  notification.style.display = 'block';
  input.classList.add('invalid');
  button.disabled = true;
}

// Function to show the success notification
function showSuccessNotification() {
  notification.textContent = 'Data was successfully sent!';
  notification.style.display = 'block';
}

// Add event listener to the input field
input.addEventListener('input', () => {
  const phoneNumber = input.value;
  if (validatePhoneNumber(phoneNumber)) {
    input.classList.remove('invalid');
    notification.style.display = 'none';
    button.disabled = false;
  } else {
    showInvalidDataNotification();
  }
});

// Add event listener to the button
button.addEventListener('click', () => {
  showSuccessNotification();
});
/* END TASK 2 */

/* START TASK 3: Your code goes here */

/* END TASK 3 */
