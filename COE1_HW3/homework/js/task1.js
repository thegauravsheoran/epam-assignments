// Your code goes here
// Step 1: Prompt the user for the initial amount
let initialAmount = parseInt(prompt('Enter the initial amount of money:'));

// Step 2: Prompt the user for the number of years
let numberOfYears = parseInt(prompt('Enter the number of years:'));

// Step 3: Prompt the user for the percentage of a year
let percentageOfYear = parseInt(prompt('Enter the percentage of a year:'));

// Step 4: Validate the user input
if (isNaN(initialAmount) || isNaN(numberOfYears) ||
 isNaN(percentageOfYear) || initialAmount < 1000 || numberOfYears < 1 || percentageOfYear > 100) {
  alert('Invalid input data');
} else {
  
  // Step 6: Calculate the total profit and total amount
  let totalAmount = initialAmount;
  let totalProfit = 0;
  let percentage = percentageOfYear / 100;
  for (let i = 1; i <= numberOfYears; i++) {
    let profit = totalAmount * percentage;
    totalProfit += profit;
    totalAmount += profit;
    // Step 7: Show the message for each year
    alert(i + ' Year\nTotal profit: ' + profit.toFixed(2) + '\nTotal amount: ' + totalAmount.toFixed(2));
  }
  // Show the final message
  alert('Initial amount: ' + initialAmount.toFixed(2) +
   '\nNumber of years: ' + numberOfYears + '\nPercentage of year: ' + 
   percentageOfYear + '\nTotal profit: ' + totalProfit.toFixed(2) + '\nTotal amount: '
    + totalAmount.toFixed(2));
}
